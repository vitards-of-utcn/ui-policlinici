


import java.awt.event.*;



public class InspectorController extends ControllerGeneric{
//... The Controller needs to interact with both the Model and View.
private InspectorModel m_model;
private InspectorView  m_view;

//========================================================== constructor
/** Constructor */
InspectorController(ModelGeneric model, ViewGeneric view) {
  m_model = (InspectorModel) model;
  m_view  = (InspectorView) view;
  
  
  //... Add listeners to the view.
  m_view.addLogOutListener(new LogOutListener());
  
}


class LogOutListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        
        try {
        	m_model.logOut();
            
        } catch (NumberFormatException nfex) {
           
        }
    }
}




}
