

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class InspectorView extends ViewGeneric {
//... Components
	
private JButton m_cautare = new JButton("Cautare date angajati");
private JButton m_consultareOrare = new JButton("Consultare orare");
private JButton m_consultareConcedii = new JButton("Consultare concedii");
private JButton m_inregistrareConcediu = new JButton("Inregistrare concediu");

private JButton m_logOut = new JButton("Log Out");

private InspectorModel m_model;

//======================================================= constructor
/** Constructor */
InspectorView(ModelGeneric model) {
  //... Set up the logic
	m_model = (InspectorModel) model;
	
  
  JPanel content = new JPanel();
  content.setLayout(new FlowLayout());
  content.add(new JLabel("Nume si Prenume: "));
  content.add(new JLabel("Functie: Inspector Resurse Umane"));
  content.add(new JLabel("Administrator: "));
  content.add(new JLabel("Super-Administrator: "));
  
  content.add(m_cautare);
  content.add(m_consultareOrare);
  content.add(m_consultareConcedii);
  content.add(m_inregistrareConcediu);
  content.add(m_logOut);
  
  this.setContentPane(content);
  this.pack();
  
  this.setTitle("Inspector");
  // The window closing event should probably be passed to the 
  // Controller in a real program, but this is a short example.
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}


void addCautareListener(ActionListener mal) {
	m_cautare.addActionListener(mal);
}

void addConsultareOrareListener(ActionListener mal) {
	m_consultareOrare.addActionListener(mal);
}

void addConsultareConcediiListener(ActionListener mal) {
	m_consultareConcedii.addActionListener(mal);
}

void addIndregistrareConcediuListener(ActionListener mal) {
	m_inregistrareConcediu.addActionListener(mal);
}

void addLogOutListener(ActionListener mal) {
	m_logOut.addActionListener(mal);
}


void showError(String errMessage) {
  JOptionPane.showMessageDialog(this, errMessage);
}


}
