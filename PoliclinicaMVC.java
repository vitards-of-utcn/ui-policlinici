
import java.awt.event.WindowEvent;

import javax.swing.*;

public class PoliclinicaMVC {
	public static String paginaCurenta = "Login";
	public static ViewGeneric lastView;
    //... Create model, view, and controller.  They are
    //    created once here and passed to the parts that
    //    need them so there is only one copy of each.
    public static void main(String[] args) {
    	
    	ModelGeneric model = null;
    	ViewGeneric view = null ;
    	ControllerGeneric controller = null;
    	
   
    	
    	if(paginaCurenta.equals("Login")) {
        		model     = new PoliclinicaModel();
                view     = new PoliclinicaView(model);
                controller = new PoliclinicaController(model, view);
                view.setVisible(true);
        }else if(paginaCurenta.equals("Inspector")) {
        		model     = new InspectorModel();
                view     = new InspectorView(model);
                controller = new InspectorController(model, view);
                view.setVisible(true);
        	}
    	
    	lastView = view;
    	
  
         
         /*switch(paginaCurenta) {
         case "Login":
           // code block
        	 PoliclinicaModel      model     = new PoliclinicaModel();
             PoliclinicaView       view     = new PoliclinicaView(model);
             PoliclinicaController controller = new PoliclinicaController(model, view);
           break;
         case "Inspector":
           // code block
        	 InspectorModel      model     = new InspectorModel();
             InspectorView       view     = new InspectorView(model);
             InspectorController controller = new InspectorController(model, view);
           break;
         default:
           // code block
       }*/

       
    }
}
