// CalcView.java - View component
//    Presentation only.  No user actions.
// Fred Swartz -- December 2004

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

class PoliclinicaView extends ViewGeneric {
    //... Components
    private JTextField m_userName = new JTextField(13);
    private JTextField m_userPassword = new JTextField(15);
    
    private JButton    m_loginBtn = new JButton("Login");

    private PoliclinicaModel m_model;
    
    //======================================================= constructor
    /** Constructor */
    PoliclinicaView(ModelGeneric model) {
        //... Set up the logic
    	m_model = (PoliclinicaModel) model;
    	
        m_userName.setEditable(true);
        m_userPassword.setEditable(true);
      
        
        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("User Name"));
        content.add(m_userName);
        content.add(new JLabel("Password"));
        content.add(m_userPassword);
        
        content.add(m_loginBtn);
        
        this.setContentPane(content);
        this.pack();
        
        this.setTitle("Policlinica");
        // The window closing event should probably be passed to the 
        // Controller in a real program, but this is a short example.
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    String getUserNameInput() {
        return m_userName.getText();
    }

    String getPasswordInput() {
        return m_userPassword.getText();
    }
    
    void addLoginListener(ActionListener mal) {
    	m_loginBtn.addActionListener(mal);
    }
    
    void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }
 
    
}
