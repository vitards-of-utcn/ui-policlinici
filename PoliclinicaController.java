// CalcController.java - Controller
//    Handles user interaction with listeners.
//    Calls View and Model as needed.
// Fred Swartz -- December 2004

import java.awt.event.*;

public class PoliclinicaController extends ControllerGeneric{
    //... The Controller needs to interact with both the Model and View.
    private PoliclinicaModel m_model;
    private PoliclinicaView  m_view;
    
    //========================================================== constructor
    /** Constructor */
    PoliclinicaController(ModelGeneric model, ViewGeneric view) {
        m_model = (PoliclinicaModel) model;
        m_view  = (PoliclinicaView) view;
        
        m_view.addLoginListener(new LoginListener());
        
        //... Add listeners to the view.
      
        
    }
    
    
    ////////////////////////////////////////// inner class MultiplyListener

    class LoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userName = "";
            String password = "";
            int rezultat = 0;
            try {
                userName = m_view.getUserNameInput();
                password = m_view.getPasswordInput();
                m_model.login(userName, password, rezultat);
                
            } catch (NumberFormatException nfex) {
                m_view.showError("Bad input: '" + userName + "'" + password);
            }
        }
    }//end inner class addListener
    
    
    

}
